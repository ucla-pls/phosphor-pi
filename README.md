Phosphor-PI: Partial Instrumentation with Phosphor
========


This repository contains the code and instructions for running Phosphor-PI, our fork of [Phosphor](https://github.com/Programming-Systems-Lab/Phosphor).  Phosphor-PI aims to improve the runtime performance of Phosphor by using static analysis to identify which relevant subset of all methods should be instrumented.


Running
-------

To use Phosphor-PI, we will use the unmodified version of Phosphor to instrument the JRE, use Phosphor-PI to instrument your bytecode, and then run your bytecode.

We'll assume that in all of the code examples below, we're in the same directory (which has a copy of [phosphor.jar](https://bitbucket.org/ucla-pls/phosphor-pi/raw/master/phosphor.jar) and [phosphor-pi.jar](https://bitbucket.org/ucla-pls/phosphor-pi/raw/master/phosphor-pi.jar)).

To instrument the JRE, we consult the Phosphor README:

  > Phosphor works by modifying your application's bytecode to perform data flow tracking. To be complete, Phosphor also modifies the bytecode of JRE-provided classes, too. The first step to using Phosphor is generating an instrumented version of your runtime environment. We have tested Phosphor with versions 7 and 8 of both Oracle's HotSpot JVM and OpenJDK's IcedTea JVM.

  > We assume that the JRE is located here: `$JAVA_HOME/jre` (modify this path in the commands below to match your environment).

  > Then, to instrument the JRE we'll run: `java -jar phosphor.jar $JAVA_HOME/jre jre-inst`

Phosphor takes two primary arguments: first, a path containing the classes to instrument, and then a destination for the instrumented classes.

Phosphor-PI takes an additional argument: a methods file which tells Phosphor-PI exactly which methods to instrument.

The next step is to instrument the code you want to track. We provide a zip file containing 8 of the 14 DaCapo benchmarks as precompiled jars and associated pre-computed methods files for each of the benchmarks. You can download it [here](https://bitbucket.org/ucla-pls/phosphor-pi/downloads/dacapo-pi.zip) or see the Downloads section.

Let's instrument the Avrora benchmark.  Assume that the zip file was extracted to `$DACAPO`. Phosphor will try to find a methods file in the current directory.  First, run `cp $DACAPO/avrora-methods ./methods`.  Then, to instrument, run `java -jar phosphor-pi.jar $DACAPO/avrora avrora-inst`

This will create the folder avrora-inst, and place in it the instrumented version of Avrora.

We can now run instrumented version of Avrora using our instrumented JRE, as such:
`JAVA_HOME=jre-inst/ $JAVA_HOME/bin/java -Xbootclasspath/a:phosphor-pi.jar -cp avrora-inst Harness avrora` (Harness is the main class and 'avrora' is a required argument of the DaCapo test harness).

Generating Methods Files
-----
If you would like to perform your own static analysis to generate the methods files, you can use [Petablox](https://github.com/petablox-project/petablox), a declarative program analysis tool by Georgia Tech.

License
-------
This software is released under the MIT license.

Copyright (c) 2013, by The Trustees of Columbia University in the City of New York.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Acknowledgements
--------

The authors of Phosphor are [Jonathan Bell](http://jonbell.net) and [Gail Kaiser](http://www.cs.columbia.edu/~kaiser/). The authors are members of the [Programming Systems Laboratory](http://www.psl.cs.columbia.edu/).

The authors of Phosphor-PI are Manoj Thakur and Joe Cox.
